import { Request } from 'express';

export interface IPropCreatedAt {
  createdAt?: Date;
}

export interface IPropUpdatedAt {
  updatedAt?: Date;
}

export type RequestWithBody<T> = Request<any, any, T>;
export type RequestWithQuery<T> = Request<any, any, any, T>;
export type RequestWithParams<T> = Request<T>;
