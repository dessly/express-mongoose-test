export interface PaginationResponseObject<TItem> {
  data: TItem[];
  total: number;
}
