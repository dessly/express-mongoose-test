import { IsString, IsNotEmpty, IsEnum, IsMongoId } from 'class-validator';
import { IReaction, ReactionType } from '@/models/reaction.model';

export class AddReactionDto implements Required<Omit<IReaction, 'reading'>> {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  reading: string;

  @IsEnum(ReactionType)
  type: ReactionType;
}
