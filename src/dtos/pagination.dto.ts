import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

export class PaginationDto {
  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  take: number;

  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  skip: number;
}
