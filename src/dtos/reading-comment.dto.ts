import { IsString, IsNotEmpty, ValidateNested, MaxLength, MinLength } from 'class-validator';
import { IReadingComment, IReadingCommentUser } from '@/models/reading-comment.model';
import { Type } from 'class-transformer';

export class ReadingCommentUserDto implements IReadingCommentUser {
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(100)
  fullName: string;
}

export class CreateReadingCommentDto implements Required<Omit<IReadingComment, 'readingId' | 'likesCounter'>> {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => ReadingCommentUserDto)
  createdBy: IReadingCommentUser;

  @IsNotEmpty()
  @IsString()
  @MaxLength(500)
  @MinLength(3)
  content: string;
}
