import { NextFunction, Response, Request } from 'express';
import { Container } from 'typedi';
import { isValidMongoObjectId } from '@/utils/dto';
import { ReadingCommentLikeService } from '@/services/reading-comment-like.service';

export class ReadingCommentLikeController {
  public service = Container.get(ReadingCommentLikeService);

  public createOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const commentId: string = req.params.commentId;

      isValidMongoObjectId(commentId);

      const createOne = await this.service.createOne(commentId);

      res.status(201).json({ data: createOne, message: 'createOne' });
    } catch (error) {
      next(error);
    }
  };

  public deleteOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id: string = req.params.likeId;

      isValidMongoObjectId(id);

      const deleteOne = await this.service.deleteOne(id);

      res.status(200).json({ data: deleteOne, message: 'deleteOne' });
    } catch (error) {
      next(error);
    }
  };
}
