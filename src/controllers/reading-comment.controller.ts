import { NextFunction, Response } from 'express';
import { Container } from 'typedi';
import { ReadingCommentService } from '@/services/reading-comment.service';
import { isValidMongoObjectId } from '@/utils/dto';
import { CreateReadingCommentDto } from '@/dtos/reading-comment.dto';
import { RequestWithBody, RequestWithQuery } from '@/interfaces/common.interface';
import { Types } from 'mongoose';
import { PaginationDto } from '@/dtos/pagination.dto';
import { validateDTO } from '@/utils/validate-dto';

export class ReadingCommentController {
  public service = Container.get(ReadingCommentService);

  public getAllByReadingId = async (req: RequestWithQuery<PaginationDto>, res: Response, next: NextFunction) => {
    try {
      const readingId: string = req.params.id;

      await validateDTO(PaginationDto, req.query);

      const findAll = await this.service.findAllByReadingId(readingId, req.query);

      res.status(200).json({ data: findAll, message: 'getAllByReadingId' });
    } catch (error) {
      next(error);
    }
  };

  public createOne = async (req: RequestWithBody<CreateReadingCommentDto>, res: Response, next: NextFunction) => {
    try {
      const readingId: string = req.params.id;

      isValidMongoObjectId(readingId);

      const createOne = await this.service.createOne(Object.assign(req.body, { readingId: new Types.ObjectId(readingId) }));

      res.status(201).json({ data: createOne, message: 'createOne' });
    } catch (error) {
      next(error);
    }
  };
}
