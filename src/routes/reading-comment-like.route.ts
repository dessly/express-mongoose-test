import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import { ReadingCommentLikeController } from '@/controllers/reading-comment-like.controller';

export class ReadingCommentLikeRoute implements Routes {
  public path = '/api/comment/like';
  public router = Router();
  public controller = new ReadingCommentLikeController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/:commentId`, this.controller.createOne);
    this.router.delete(`${this.path}/:likeId`, this.controller.deleteOne);
  }
}
