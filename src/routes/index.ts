import { ReadingRoute } from '@routes/reading.route';
import { ReadingCommentRoute } from './reading-comment.route';
import { ReadingCommentLikeRoute } from './reading-comment-like.route';

const routes = [new ReadingRoute(), new ReadingCommentRoute(), new ReadingCommentLikeRoute()];

export default routes;
