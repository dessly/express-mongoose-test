import { Router } from 'express';
import { ValidationMiddleware } from '@middlewares/validation.middleware';
import { Routes } from '@interfaces/routes.interface';
import { ReadingCommentController } from '@/controllers/reading-comment.controller';
import { CreateReadingCommentDto } from '@/dtos/reading-comment.dto';

export class ReadingCommentRoute implements Routes {
  public path = '/api/reading/:id';
  public router = Router();
  public controller = new ReadingCommentController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/comments`, this.controller.getAllByReadingId);
    this.router.post(`${this.path}/comment`, ValidationMiddleware(CreateReadingCommentDto), this.controller.createOne);
  }
}
