import { Service } from 'typedi';
import { ReadingCommentModel } from '@/models/reading-comment.model';
import { HttpException } from '@/exceptions/httpException';
import { ReadingCommentLike, ReadingCommentLikeModel } from '@/models/reading-comment-like.model';
import { Types } from 'mongoose';

@Service()
export class ReadingCommentLikeService {
  public async createOne(commentId: string): Promise<ReadingCommentLike> {
    const updatedComment = await ReadingCommentModel.findOneAndUpdate(
      { _id: new Types.ObjectId(commentId) },
      { $inc: { likesCounter: 1 } },
      { new: true },
    );

    if (!updatedComment) {
      throw new HttpException(404, 'Comment not found');
    }

    const readingCommentLike = await ReadingCommentLikeModel.create({ commentId });

    return readingCommentLike;
  }

  public async deleteOne(id: string): Promise<ReadingCommentLike> {
    const deleteOneById = await ReadingCommentLikeModel.findByIdAndDelete({ _id: new Types.ObjectId(id) });
    if (!deleteOneById) {
      throw new HttpException(409, `Comment with id "${id}" doesn't exist`);
    }

    await ReadingCommentModel.findByIdAndUpdate(deleteOneById.commentId, { $inc: { likesCounter: -1 } });

    return deleteOneById;
  }
}
