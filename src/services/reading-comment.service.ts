import { Service } from 'typedi';
import { IReadingComment, ReadingComment, ReadingCommentModel } from '@/models/reading-comment.model';
import { ReadingModel } from '@/models/reading.model';
import { HttpException } from '@/exceptions/httpException';
import { PaginationDto } from '@/dtos/pagination.dto';
import { PaginationResponseObject } from '@/interfaces/pagination.interface';

@Service()
export class ReadingCommentService {
  public async findAllByReadingId(id: string, { take, skip }: PaginationDto): Promise<PaginationResponseObject<ReadingComment>> {
    const where = { readingId: id };

    return Promise.all([
      ReadingCommentModel.find(where).sort({ createdAt: -1 }).skip(skip).limit(take),
      ReadingCommentModel.countDocuments(where),
    ]).then(([data, total]) => ({
      data,
      total,
    }));
  }

  public async createOne(data: IReadingComment): Promise<ReadingComment> {
    const reading = await ReadingModel.findByIdAndUpdate(data.readingId, { $inc: { commentsCounter: 1 } }, { new: true });
    if (!reading) {
      throw new HttpException(404, 'Reading not found');
    }

    return ReadingCommentModel.create(data);
  }
}
