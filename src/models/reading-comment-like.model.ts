import { HydratedDocument, Model, model, Schema, Types } from 'mongoose';

export interface IReadingCommentLike {
  commentId: Types.ObjectId;
}

export type IReadingCommentLikeModel = Model<IReadingCommentLike, {}>;

export type ReadingCommentLike = HydratedDocument<IReadingCommentLike>;

const readingCommentLikeSchema = new Schema<IReadingCommentLike>(
  {
    commentId: {
      type: Schema.Types.ObjectId,
      ref: 'ReadingComment',
      required: true,
    },
  },
  { timestamps: true },
);

export const ReadingCommentLikeModel = model<ReadingCommentLike>('ReadingCommentLike', readingCommentLikeSchema);
