import { HydratedDocument, Model, model, Schema, Types } from 'mongoose';

export enum ReactionType {
  Like = 'like',
  Heart = 'heart',
  Fire = 'fire',
  Explosion = 'explosion',
}

export interface IReaction {
  reading: Types.ObjectId;
  type: ReactionType;
}

export type IReactionModel = Model<IReaction, {}>;

export type Reaction = HydratedDocument<IReaction>;

const reactionSchema = new Schema<IReaction>(
  {
    reading: {
      type: Schema.Types.ObjectId,
      ref: 'Reading',
      required: true,
    },
    type: {
      type: String,
      required: true,
      enum: Object.values(ReactionType),
    },
  },
  { timestamps: true },
);

export const ReactionModel = model<IReaction>('Reaction', reactionSchema);
