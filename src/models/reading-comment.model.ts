import { HydratedDocument, Model, model, Schema, Types } from 'mongoose';

export interface IReadingCommentUser {
  fullName: string;
}

export interface IReadingComment {
  readingId: Types.ObjectId;
  createdBy: IReadingCommentUser;
  content: string;
  likesCounter?: number;
}

export type IReadingCommentModel = Model<IReadingComment, {}>;

export type ReadingComment = HydratedDocument<IReadingComment>;

const readingCommentSchema = new Schema<IReadingComment>(
  {
    content: { type: String, required: true },
    createdBy: {
      fullName: { type: String, required: true },
    },
    readingId: { type: Schema.Types.ObjectId, ref: 'Reading', required: true },
    likesCounter: {
      required: true,
      type: Number,
      default: 0,
      validate: {
        validator: Number.isInteger,
        message: prop => `propPath: ${prop.path}; propValue:${prop.value}; Is not an integer value!`,
      },
    },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);

export const ReadingCommentModel = model<IReadingComment>('ReadingComment', readingCommentSchema);
