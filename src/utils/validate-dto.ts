import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { HttpException } from '@/exceptions/httpException';

export async function validateDTO<T>(cls: new () => T, obj: any) {
  const instance = plainToInstance(cls, obj);

  const errors = await validate(instance as any);

  if (errors.length)
    throw new HttpException(
      400,
      errors
        .reduce((acc, err) => {
          const constraints = err.constraints ? Object.values(err.constraints) : [];
          return acc.concat(constraints);
        }, [])
        .join(', '),
    );
}
